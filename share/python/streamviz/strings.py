# all constant strings

settings = {}

settings["api"] = {}
settings["api"]["recieve_url"] = '/cfs_receive'
settings["api"]["view_url"] = '/view'
settings["api"]["plot_url"] = '/plot'
settings["api"]["values"] = '/values'
settings["api"]["view_results"] = '/view_result'
settings["api"]["catalyst_send"] = '/catalyst_send'

settings["html_template"] = {}
settings["html_template"]["key_menu"] = "MENU_MARKER"
settings["html_template"]["key_content"] = "CONTENT_MARKER"
